<a name="3.2.0"></a>
# [3.2.0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.1.1...v3.2.0) (2019-11-06)


### Bug Fixes

* make dark text actually darker than ordinary text ([aa4762c](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/aa4762c))


### Features

* enable inverted alpha colours ([f567236](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/f567236))



<a name="3.1.1"></a>
## [3.1.1](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.1.0...v3.1.1) (2019-06-03)


### Bug Fixes

* use common gray ([3033a35](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/3033a35))



<a name="3.1.0"></a>
# [3.1.0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0...v3.1.0) (2019-02-12)



<a name="3.0.0"></a>
# [3.0.0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-7...v3.0.0) (2019-02-12)


### Features

* add custom properties of colors ([6c8564a](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/6c8564a)), closes [#19](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/issue/19)



<a name="3.0.0-7"></a>
# [3.0.0-7](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-6...v3.0.0-7) (2018-11-28)


### Bug Fixes

* **Header:** change background color of attached headers ([d26ad03](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/d26ad03))
* **Variables:** set greyBackground to a lighter gray ([a8a7e4e](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/a8a7e4e))



<a name="3.0.0-6"></a>
# [3.0.0-6](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-5...v3.0.0-6) (2018-11-22)



<a name="3.0.0-5"></a>
# [3.0.0-5](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-4...v3.0.0-5) (2018-11-20)



<a name="3.0.0-4"></a>
# [3.0.0-4](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-3...v3.0.0-4) (2018-11-19)


### Bug Fixes

* basic tables do not use dividing lines ([e634339](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/e634339))



<a name="3.0.0-3"></a>
# [3.0.0-3](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-1...v3.0.0-3) (2018-11-19)


### Bug Fixes

* header font weights ([d06bd58](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/d06bd58))
* improved tables, closer to what we use and better stacking ([c39f9d7](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/c39f9d7))



<a name="3.0.0-2"></a>
# [3.0.0-2](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-1...v3.0.0-2) (2018-11-07)


### Bug Fixes

* header font weights ([d06bd58](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/d06bd58))



<a name="3.0.0-1"></a>
# [3.0.0-1](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v3.0.0-0...v3.0.0-1) (2018-11-02)


### Bug Fixes

* do not muck with font smoothing ([3ff4774](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/3ff4774))



<a name="3.0.0-0"></a>
# [3.0.0-0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v2.0.4...v3.0.0-0) (2018-10-25)


### Features

* add css custom properties for font stacks ([2f9b745](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/2f9b745))
* replace fonts, use generic font names ([33f0c92](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/33f0c92))
* upgrade to Semantic UI 2.4.2 ([b96d982](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/b96d982))



<a name="2.0.4"></a>
## [2.0.4](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v2.0.3...v2.0.4) (2018-09-20)


### Bug Fixes

* less letter spacing for sub headings ([fac1f46](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/fac1f46))



<a name="2.0.3"></a>
## [2.0.3](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v2.0.2...v2.0.3) (2018-09-20)


### Bug Fixes

* no more capitalized .ui.sub.headers ([aa2e019](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/aa2e019))



<a name="2.0.2"></a>
## [2.0.2](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v2.0.0...v2.0.2) (2018-09-20)


### Bug Fixes

* use sans serif font in message headers ([9259215](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/9259215))
* use version-dependent source directories for icon fonts ([a3473d7](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/a3473d7))



<a name="2.0.1"></a>
## [2.0.1](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v2.0.0...v2.0.1) (2018-09-13)


### Bug Fixes

* use version-dependent source directories for icon fonts ([a3473d7](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/a3473d7))



<a name="2.0.0"></a>
# [2.0.0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.3.1...v2.0.0) (2018-09-12)


### Bug Fixes

* Use correct protocol for loading web fonts ([8a909f7](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/8a909f7))


### Features

* update Semantic UI ([7bdbc72](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/7bdbc72)), closes [/github.com/Semantic-Org/Semantic-UI/blob/master/RELEASE-NOTES.md#version-230---feb-20-2018](https://bitbucket.org//github.com/Semantic-Org/Semantic-UI/blob/master/RELEASE-NOTES.md/issue/version-230---feb-20-2018)


### BREAKING CHANGES

* New icons and icon names since



<a name="1.3.1"></a>
## [1.3.1](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.3.0...v1.3.1) (2018-03-02)


### Bug Fixes

* restyle basic labeled icon buttons ([63ce6f9](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/63ce6f9))



<a name="1.3.0"></a>
# [1.3.0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.14...v1.3.0) (2018-02-08)


### Features

* use various classes to set different font families ([dab285e](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/dab285e))



<a name="1.2.14"></a>
## [1.2.14](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.13...v1.2.14) (2018-01-16)


### Bug Fixes

* remove overflow workaround ([d4e6eb9](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/d4e6eb9))



<a name="1.2.13"></a>
## [1.2.13](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.12...v1.2.13) (2017-11-27)


### Bug Fixes

* **Button:** remove borders of attached buttons ([55a0afa](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/55a0afa))
* **Byline:** fixed author/byline styling ([afe0398](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/afe0398))
* **Header:** Change header and lead distance and lead color ([995be6f](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/995be6f))
* **Table:** reduce line height in tables ([b7f330c](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/b7f330c))



<a name="1.2.12"></a>
## [1.2.12](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.11...v1.2.12) (2017-10-30)


### Bug Fixes

* **Segment:** add minimum padding to vertical segments for grids and cards ([2d35771](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/2d35771))



<a name="1.2.11"></a>
## [1.2.11](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.10...v1.2.11) (2017-10-30)


### Bug Fixes

* **Container:** remove 13px gutter for mobile containers ([2caeee4](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/2caeee4))


### Features

* **Segment:** define vertical padding as vw, to make it responsive ([06903be](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/06903be))



<a name="1.2.10"></a>
## [1.2.10](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/v1.2.9...v1.2.10) (2017-09-25)


### Fix

* Change autoprefixer config to support more older browsers. ([7507dcefdeefeca55740d782b0aa0603f724a039](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/7507dcefdeefeca55740d782b0aa0603f724a039))



<a name="1.2.9"></a>
## [1.2.9](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/1.2.8...1.2.9) (2017-09-06)


### Chore

* Add prepublish script that builds Semantic UI ([3e46ccdfd416c204a1da7ba32b64fc39ebb85c99](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/3e46ccdfd416c204a1da7ba32b64fc39ebb85c99))
* Experimental support for automatically git tagging based on package.json ([b47c01af145dcc4e3ae28616d8132a516c437348](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/b47c01af145dcc4e3ae28616d8132a516c437348))
* Renamed NPM package ([dd37de0bec5644920c8d7e96e2d3786f99930d58](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/dd37de0bec5644920c8d7e96e2d3786f99930d58))
* Updated dependencies ([f6498e2a4dc839dfe232914f3859bfa8b2ebbd69](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/f6498e2a4dc839dfe232914f3859bfa8b2ebbd69))

### Fix

* Limit first child of body to screen width to avoid wobbling ([3a76ad336fa16525c16112d77e2a8872c300d276](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/3a76ad336fa16525c16112d77e2a8872c300d276))
* Reordered the placement of inverted and global visibility styles ([51a30e75e22fe93091abc49e9a9a40c3ae8a8008](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/51a30e75e22fe93091abc49e9a9a40c3ae8a8008))

### Tooling

* Improved version tagging scripts. ([a4d66da652a5bd527d1da86449c9b3ec8c9e25e0](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/a4d66da652a5bd527d1da86449c9b3ec8c9e25e0))



<a name="1.2.8"></a>
## [1.2.8](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/compare/1.2.7...1.2.8) (2017-08-04)


### Chore

* Added conventional changelog as dependency for quick changelog generation ([bde55d2c13a307f7ac06dd76b85ea5257eba4f6c](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/bde55d2c13a307f7ac06dd76b85ea5257eba4f6c))
* Update Readme. ([ae7e718ebe1c6d1503357c56041c1a1cfd94eb37](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/ae7e718ebe1c6d1503357c56041c1a1cfd94eb37))
* Updated dependencies ([3083fd96e46f6c5afe57632e3a73a93ef5eabb69](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/3083fd96e46f6c5afe57632e3a73a93ef5eabb69))

### Fix

* Remove horizontal padding in stacked tables. ([539001acc00d3caa8dfb39775d1abcc5e246bd8e](https://bitbucket.org/tagesanzeiger/ta-semantic-ui/commits/539001acc00d3caa8dfb39775d1abcc5e246bd8e))

## 1.2.7 – 2017-07-11
### Fixes 
- Fix uppercase lettering in dividers
- Fix link color in inverted objects
- Make dropdown text blue to mark interactivity
- Remove background in header column in definition tables

## 1.2.6 – 2017-06-07
### Fixes
- Fixed Source Sans Pro font weights (will now use 400 (Regular) and 700 (Bold))

## 1.2.5 – 2017-05-15
### Fixes
- Removed text hyphenation.

## 1.2.4 – 2017-05-12
### Changes
- Updated scaling of headers and sub headers. `h1.ui.header` will now correspond to the longform format header.

### Fixes
- `.ta.caption` should now have a proper top margin after images.

## 1.2.3 - 2017-03-27
### Fixes
- Improve line height and margins of caption elements.

## 1.2.2 – 2017-03-07
### Fixes
- Vertical segments retain padding even when being first or last.
- Coloured vertical segments don't have a coloured border anymore.
- Coloured headers now get proper dividing lines.

## 1.2.1 – 2017-03-02
- Update to Semantic UI 2.2.9

## 1.2.0 – 2017-02-07
### New
- Global classes to hide or show elements depending on device width.
- Typographic rules scoped under `.ta` – like `.ta.lead` for leads, `.ta.author` for bylines.
- `.ui.padded.segment` is now aliased under `.ui.relaxed.segment`, in case you forgot it was called «padded». (Which happens to me a lot.)
- Text in `.ui.text.container` is now being hyphenated.

### Fixed
- Grids are now no bigger than the screen width – this resolves the weird issue where scrolling sideways was possible
- Inverted text is now rendered with a different antialiasing algorithm to look more crisp (especially for Benton Cond Black).


## 1.1.1 – 2017-01-23 [Only as @next version]
### Changed
- Scaling of header sizes has been toned down, so the biggest titles aren't as big anymore.

## 1.1.0 – 2016-11-28
### Changed
- Breakpoints are now at 600px, 900px and 1200px; according to [this article][1].
This means that all containers in an article can now be targeted specifically.
- `.ui.container` will now fluidly extend to the full extent of the page up 
to a width of 1200px. As opposed to `.ui.fluid.container`, it will have a 
margin, though.

[1]: https://medium.freecodecamp.com/the-100-correct-way-to-do-css-breakpoints-88d6a5ba1862#.i48e2ocup

## 1.0.11 – 2016-11-16
### Changed
- Attached segments are now clearly visible as segments/boxes – without the borders.

### Fix
- Made the light grey segment a lighter shade of grey.
- Removed the min-width for the `body` element.

## 1.0.10 – 2016-10-20
### New
- Add readme and changelog.

### Changes
- Slightly larger base font size, based on the standard tagesanzeiger.ch font
 size.
- Updated social button brand colours to be the same as the colours of 
sharing buttons on tagesanzeiger.ch.

## 1.0.9 – 2016-10-19

- Updated button styling to conform to the art direction. Standard buttons 
now use blue text to denote interactivity; basic buttons will be styled as 
"text links" and only get a button shape upon hovering.