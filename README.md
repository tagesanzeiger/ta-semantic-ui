# Semantic UI

These are stylesheets used for interactive visualisations for 
tagesanzeiger.ch and other paid media titles by Tamedia.

The styles are based on Semantic UI and adapted to conform to the art 
direction.

## Usage within React

### Installation

Yarn:
~~~bash
yarn add @ta-interaktiv/semantic-ui @ta-interaktiv/react-theme-switch
~~~

NPM:
~~~bash
npm install @ta-interaktiv/semantic-ui @ta-interaktiv/react-theme-switch
~~~

### Inclusion

Base styles that should be included in `main.js`:
~~~js
import '@ta-interaktiv/semantic-ui/semantic/dist/components/reset.css';
import '@ta-interaktiv/semantic-ui/semantic/dist/components/site.css';
~~~

Also include the React Theme Switch component in your `main.js`, so the 
correct fonts are being used:

```jsx harmony
import React from 'react'
import { ThemeSwitch } from '@ta-interaktiv/react-theme-switch'

export const Main = () => (
  <div className='index'>
    <ThemeSwitch />
  </div>
)
```

Refer to the [Theme Switch Documentation](https://www.npmjs.com/package/@ta-interaktiv/react-theme-switch) for further instructions.

In all other react components, only the necessary Semantic UI components 
should be included, like this:

~~~js
import '@ta-interaktiv/semantic-ui/semantic/dist/components/button.css';
import '@ta-interaktiv/semantic-ui/semantic/dist/components/image.css';
import '@ta-interaktiv/semantic-ui/semantic/dist/components/icon.css';
import '@ta-interaktiv/semantic-ui/semantic/dist/components/segment.css';
~~~

## Accessing font stacks

This version of Semantic UI has four different font stacks exposed as CSS 
custom properties. Since the theming functionality mostly works over the use of
different fonts, direct references to Benton Sans Cond or similar should be 
avoided. Instead do the following:

```css
/* Chart label that should be using the chart / infographic font */
.chart-label {
    font-family: var(--chart-font-stack);
}

/* Header class that should use the text header font */
.header {
    font-family: var(--display-font-stack);
}
```

The following font stacks are available:

| Custom Property Name | Usage | Original font for tagesanzeiger.ch |
|----------------------|-------|------------------------------------|
| `--text-font-stack` | Body text | Georgia |
| `--display-font-stack` | Article headers | Publico Headline |
| `--chart-font-stack` | Infographics, charts, side bars | Benton Sans Cond |
| `--infographic-font-stack` | Alias of `--chart-font-stack` | see above |
| `--ui-font-stack` | UI elements like buttons, form fields | Source Sans and/or Benton Sans Cond |

## Using Colors

The colors as defined by `@ta-interaktiv/semantic-ui` and the infographics 
department are also available as CSS custom properties.

By default these are

```LESS
// Base Colors
--primary: @primaryColor;
--secondary: @secondaryColor;
--red: @red;
--orange: @orange;
--yellow: @yellow;
--olive: @olive;
--green: @green;
--teal: @teal;
--blue: @blue;
--violet: @violet;
--purple: @purple;
--pink: @pink;
--brown: @brown;
--grey: @grey;
--black: @black;

// Light Colors
--light-red: @redBackground;
--light-orange: @orangeBackground;
--light-yellow: @yellowBackground;
--light-olive: @oliveBackground;
--light-green: @greenBackground;
--light-teal: @tealBackground;
--light-blue: @blueBackground;
--light-violet: @violetBackground;
--light-purple: @purpleBackground;
--light-pink: @pinkBackground;
--light-brown: @brownBackground;

// Text Colors
--red-text-color: @redTextColor;
--orange-text-color: @orangeTextColor;
--yellow-text-color: @yellowTextColor;
--olive-text-color: @oliveTextColor;
--green-text-color: @greenTextColor;
--teal-text-color: @tealTextColor;
--blue-text-color: @blueTextColor;
--violet-text-color: @blueTextColor;
--purple-text-color: @purpleTextColor;
--pink-text-color: @pinkTextColor;
--brown-text-color: @brownTextColor;
--dark-text-color: @darkTextColor;
--text-color: @textColor;
--muted-text-color: @mutedTextColor;
--light-text-color: @lightTextColor;

// Alpha Colors
--subtle-transparent-black: @subtleTransparentBlack;
--transparent-black: @transparentBlack;
--strong-transparent-black: @strongTransparentBlack;
--very-strong-transparent-black: @veryStrongTransparentBlack;
--subtle-transparent-white: @subtleTransparentWhite;
--transparent-white: @transparentWhite;
--strong-transparent-white: @strongTransparentWhite;
--off-white: @offWhite;
--dark-white: @darkWhite;
--mid-white: @midWhite;  
--light-grey: #E7F1F6;
--mid-grey: #CDD2D3;

```


## Other usages
A complete Version of Semantic UI for use in other projects is here:

`http://interaktiv.tagesanzeiger.ch/static/semantic/[current version 
number]/semantic.min.css` (533KB!)
`http://interaktiv.tagesanzeiger.ch/static/semantic/[current version 
number]/semantic.min.js` (264KB!)

Separate components are available as well.